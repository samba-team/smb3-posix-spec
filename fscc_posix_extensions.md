<style>
body {
  font-family: Segoe UI,SegoeUI,Helvetica Neue,Helvetica,Arial,sans-serif;
  padding: 1em;
  margin: auto;
  max-width: 42em;
  background: #fefefe;
}
.protocol-table p {
  margin-top: 0px;
}
p {
  margin-top: 1rem;
  margin-bottom: 0px;
}
table {
  margin-top: 1rem;
  border: 1px solid #EAEAEA;
  border-collapse: collapse;
  width: 100%;
}
table, th, td {
  border-radius: 3px;
  padding: 5px;
}
th, td {
  font-size: .875rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  border: 1px solid #bbb;
  color: #2a2a2a;
}
th {
  background-color: #ededed;
  font-weight: 600;
}
td {
  padding: 0.5rem;
  background-color: #fff;
}
h1, h2, h3, h4, h5, h6 {
  font-weight: bold;
  color: #000;
}
h1 {
  font-size: 28px;
}
h2 {
  font-size: 24px;
}
h3 {
  font-size: 18px;
}
h4 {
  font-size: 16px;
}
h5 {
  font-size: 14px;
}
</style>

# [POSIX-FSCC] POSIX Extensions to MS-FSCC

Specifies the MS-FSCC extensions for supporting POSIX compliant operating
systems.

## Published Version

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p>
  </p>
  </td>
</tr></tbody></table>

## Previous Versions

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody>
 <tr>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p>
  </p>
  </td>
 </tr>
</tbody></table>

# 1 Introduction

The SMB3 POSIX Extensions are extensions to enable POSIX compliant
operating systems to better interoperate with SMB3 servers and storage
appliances. This document specified the extensions to the
[[MS-FSCC]](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc)
specification. Popular servers such as Samba, Windows Server and others
support SMB3 by default.  These extensions are already implemented in multiple
clients and servers.

# 2 Messages and Structures
## 2.3 Structures

### 2.3.1 File Information Classes

Refer to section
[2.4](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/4718fc40-e539-4014-8e33-b675af74e3e1)
of \[MS-FSCC\] for additional details. This section
only explains details relevant to the FilePosixInformation info class.

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>File information class</p>
   </th>
   <th>
   <p>Level</p>
   </th>
   <th>
   <p>Uses</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p><span><a href="#fileposixinformation">FilePosixInformation</a></span></p>
  </td>
  <td>
  <p>0x64</p>
  </td>
  <td>
  <p>Query</p>
  </td>
</tr></tbody></table>

##### 2.3.1.1 FilePosixInformation

This information class is used to query file posix information.

A FILE_POSIX_INFORMATION data element, defined as follows, is returned by the
server.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>CreationTime</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>LastAccessTime</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>LastWriteTime</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>ChangeTime</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>EndOfFile</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>AllocationSize</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FileAttributes</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Inode</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Device</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Reserved</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>NumberOfLinks</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>ReparseTag</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>POSIXPerms</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>OwnerSID
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>GroupSID
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FilenameLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Filename
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**CreationTime (8 bytes):** The time when the file was created; see section
[2.1.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf)
of \[MS-FSCC\]. A valid time for this field is an integer greater than or equal
to 0. When setting file attributes, a value of 0 indicates to the server that it
MUST NOT change this attribute. When setting file attributes, a value of -1
indicates to the server that it MUST NOT change this attribute for all
subsequent operations on the same file handle. When setting file attributes, a
value of -2 indicates to the server that it MUST change this attribute for all
subsequent operations on the same file handle. This field MUST NOT be set to a
value less than -2.

**LastAccessTime (8 bytes):** The last time the file was accessed; see section
[2.1.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf)
of \[MS-FSCC\]. A valid time for this field is an integer greater than or equal
to 0. When setting file attributes, a value of 0 indicates to the server that it
MUST NOT change this attribute. When setting file attributes, a value of -1
indicates to the server that it MUST NOT change this attribute for all
subsequent operations on the same file handle. When setting file attributes, a
value of -2 indicates to the server that it MUST change this attribute for all
subsequent operations on the same file handle. This field MUST NOT be set to a
value less than -2.

**LastWriteTime (8 bytes):** The last time information was written to the file;
see section
[2.1.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf)
of \[MS-FSCC\]. A valid time for this field is an integer greater than or equal
to 0. When setting file attributes, a value of 0 indicates to the server that
it MUST NOT change this attribute. When setting file attributes, a value of -1
indicates to the server that it MUST NOT change this attribute for all
subsequent operations on the same file handle. When setting file attributes, a
value of -2 indicates to the server that it MUST change this attribute for all
subsequent operations on the same file handle. This field MUST NOT be set to a
value less than -2.

**ChangeTime (8 bytes):** The last time the file was changed; see section
[2.1.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf)
of \[MS-FSCC\]. A valid time for this field is an integer greater than or equal
to 0. When setting file attributes, a value of 0 indicates to the server that it
MUST NOT change this attribute. When setting file attributes, a value of -1
indicates to the server that it MUST NOT change this attribute for all
subsequent operations on the same file handle. When setting file attributes, a
value of -2 indicates to the server that it MUST change this attribute for all
subsequent operations on the same file handle. This field MUST NOT be set to a
value less than -2.

**EndOfFile (8 bytes):** The size of the file.

**AllocationSize (8 bytes):** The size allocated on disk for the file.

**FileAttributes (4 bytes):** A 32-bit unsigned integer that contains the file
attributes. Valid file attributes are specified in section
[2.6](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/ca28ec38-f155-4768-81d6-4bfeb8586fc9)
of \[MS-FSCC\].

**Inode (8 bytes):** The POSIX inode for the file.

**FileId (4 bytes):** The POSIX fileId for the file.

**Reserved (4 bytes):** A 32-bit field. This field is reserved. This field can be set to any value, and MUST be ignored.

**NumberOfLinks (4 bytes):** The number of hard links to the file.

**ReparseTag (4 bytes):** The reparse tag, as defined in section
[2.1.2.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/c8e77b37-3909-4fe6-a4ea-2b9d423b1ee4)
of [MS-FSCC].

**POSIXPerms (4 bytes):** The posix permissions for the opened file as defined
in section [4.5](https://pubs.opengroup.org/onlinepubs/9699919799/)
of IEEE Std 1003.1-2017.

**OwnerSID (variable):** The owner SID as defined in
[Microsoft Security identifiers](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-identifiers).

**GroupSID (variable):** The group SID as defined in
[Microsoft Security identifiers](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-identifiers).

**FilenameLength (4 bytes):** The length, in bytes, of the filename.

**Filename (variable):** A variable-length buffer containing the filename, the
length specified by **FilenameLength**.

### 2.3.2 File System Information Classes

Refer to section
[2.5](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/ee12042a-9352-46e3-9f67-c094b75fe6c3)
of \[MS-FSCC\] for additional details. This section only explains details relevant to the FileFsPosixInformation info class.

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>File system information class</p>
   </th>
   <th>
   <p>Level</p>
   </th>
   <th>
   <p>Uses</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p><span><a href="#filefsposixinformation">FileFsPosixInformation</a></span></p>
  </td>
  <td>
  <p>0x64</p>
  </td>
  <td>
  <p>Query</p>
  </td>
</tr></tbody></table>

##### 2.3.2.1 FileFsPosixInformation

This information class is used to query file system posix information.

A FILE_FS_POSIX_INFORMATION data element, defined as follows, is returned by the server.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>OptimalTransferSize</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>BlockSize</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>TotalBlocks</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>BlocksAvailable</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>

 <tr>
  <td colspan="32">
  <p>UserBlocksAvailable</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>TotalFileNodes</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FreeFileNodes</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FsIdentifier</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**OptimalTransferSize (4 bytes)**: The preferred length of I/O requests for files on this file system., from `struct statvfs.f_bsize`.

**BlockSize (4 bytes)**: The size in bytes of the minimum unit of allocation on this filesystem, from `struct statvfs.f_frsize`.

**TotalBlocks (8 bytes)**: Size of filesystem in *BlockSize* units, from `struct statvfs.f_blocks`.

**BlocksAvailable (8 bytes)**: Number of free blocks in *BlockSize* units, from `struct statvfs.f_bfree`.

**UserBlocksAvailable (8 bytes)**: Number of free blocks for unprivileged users in *BlockSize* units, from `struct statvfs.f_bavail`.

**TotalFileNodes (8 bytes)**: Number of inodes, from `struct statvfs.f_files`.

**FreeFileNodes (8 bygtes)**: Number of free inodes, from `struct statvfs.f_ffree`.

**FsIdentifier (8 bytes)**: Filesystem ID, from `struct statvfs.f_fsid`.

