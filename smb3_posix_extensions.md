<style>
body {
  font-family: Segoe UI,SegoeUI,Helvetica Neue,Helvetica,Arial,sans-serif;
  padding: 1em;
  margin: auto;
  max-width: 42em;
  background: #fefefe;
}
.protocol-table p {
  margin-top: 0px;
}
p {
  margin-top: 1rem;
  margin-bottom: 0px;
}
table {
  margin-top: 1rem;
  border: 1px solid #EAEAEA;
  border-collapse: collapse;
  width: 100%;
}
table, th, td {
  border-radius: 3px;
  padding: 5px;
}
th, td {
  font-size: .875rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  border: 1px solid #bbb;
  color: #2a2a2a;
}
th {
  background-color: #ededed;
  font-weight: 600;
}
td {
  padding: 0.5rem;
  background-color: #fff;
}
h1, h2, h3, h4, h5, h6 {
  font-weight: bold;
  color: #000;
}
h1 {
  font-size: 28px;
}
h2 {
  font-size: 24px;
}
h3 {
  font-size: 18px;
}
h4 {
  font-size: 16px;
}
h5 {
  font-size: 14px;
}
</style>

# [POSIX-SMB2] SMB3 POSIX Extensions

Specifies the SMB3 protocol extension for supporting POSIX compliant operating
systems.

## Published Version

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p>9/27/2023</p>
  </td>
  <td>
  <p>0.03</p>
  </td>
  <td>
  <p>Minor</p>
  </td>
  <td>
  <p>
    <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.3/smb3_posix_extensions.pdf" data-linktype="external">PDF</a></span>
  | <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.3/smb3_posix_extensions.html" data-linktype="external">HTML</a></span>
  </p>
  </td>
</tr></tbody></table>

## Previous Versions

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody>
 <tr>
  <td>
  <p>10/10/2022</p>
  </td>
  <td>
  <p>0.02</p>
  </td>
  <td>
  <p>Minor</p>
  </td>
  <td>
  <p>
    <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.02/smb3_posix_extensions.pdf" data-linktype="external">PDF</a></span>
  | <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.02/smb3_posix_extensions.html" data-linktype="external">HTML</a></span>
  </p>
  </td>
 </tr>
 <tr>
  <td>
  <p>9/22/2022</p>
  </td>
  <td>
  <p>0.01</p>
  </td>
  <td>
  <p>New</p>
  </td>
  <td>
  <p>
    <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.01/smb3_posix_spec.pdf" data-linktype="external">PDF</a></span>
  | <span><a href="https://codeberg.org/SMB3UNIX/smb3_posix_spec/releases/download/0.01/smb3_posix_spec.html" data-linktype="external">HTML</a></span>
  </p>
  </td>
 </tr>
</tbody></table>

# 1 Introduction

The SMB3 POSIX Extensions are protocol extensions to enable POSIX compliant
operating systems to better interoperate with SMB3 servers and storage
appliances by extending the
[[MS-SMB2]](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2)
specification. Popular servers such as Samba, Windows Server and others
support SMB3 by default.  These extensions are already implemented in multiple
clients and servers.

# 2 Messages and Structures

## 2.2 Message Syntax

### 2.2.3 SMB2 NEGOTIATE Request

#### 2.2.3.1 SMB2 NEGOTIATE_CONTEXT Request Values

Refer to section
[2.2.3.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/15332256-522e-4a53-8cd7-0bd17678a2f7)
of \[MS-SMB2\] for additional details. This section
only explains details relevant to a SMB3_POSIX_EXTENSIONS_AVAILABLE negotiate
context request.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="16">
  <p>ContextType</p>
  </td>
  <td colspan="16">
  <p>DataLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Reserved</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Data
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**ContextType (2 bytes):** Specifies the type of context in the Data field. This
field MUST contain one of the values specified in section
[2.2.3.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/15332256-522e-4a53-8cd7-0bd17678a2f7)
of [MS-SMB2] or the following extended value:

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Value</p>
   </th>
   <th>
   <p>Meaning</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p>SMB3_POSIX_EXTENSIONS_AVAILABLE</p>
  <p>0x0100</p>
  </td>
  <td>
  <p>The <b>Data</b> field contains a blob which uniquely identifies the SMB3
  POSIX version.</p>
  </td>
</tr></tbody></table>

**DataLength (2 bytes):** The length, in bytes, of the **Data** field.

**Reserved (4 bytes):** This field MUST NOT be used and MUST be reserved. This
value MUST be set to 0 by the client, and MUST be ignored by the server.

**Data (variable):** A variable-length field that contains the negotiate context
specified by the **ContextType** field.

##### 2.2.3.1.8 SMB3_POSIX_EXTENSIONS_AVAILABLE

The SMB3_POSIX_EXTENSIONS_AVAILABLE context is specified in an SMB2 NEGOTIATE
request by the client to indicate the version of SMB3 POSIX Extensions that the
client supports. The format of the data in the **Data** field of this
SMB2_NEGOTIATE_CONTEXT is as follows.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>PosixTag</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>…</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>…</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>…</p>
  </td>
 </tr>
</table>

**PosixTag (16 bytes):** A blob indicating the SMB3 POSIX version. The following
versions are defined.

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p><span>Version</span></p>
   </th>
   <th>
   <p><span>Value</span></p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p><span>1</span></p>
  </td>
  <td>
  <p>0x93AD25509CB411E7B42383DE968BCD7C</p>
  </td>
</tr></tbody></table>

### 2.2.4 SMB2 NEGOTIATE Response

#### 2.2.4.1 SMB2 NEGOTIATE_CONTEXT Response Values

##### 2.2.4.1.8 SMB3_POSIX_EXTENSIONS_AVAILABLE

The SMB3_POSIX_EXTENSIONS_AVAILABLE context is specified in an SMB2 NEGOTIATE
response by the server to indicate that the SMB3 POSIX version specified by the
client is supported. The format of the data in the Data field of this
SMB2_NEGOTIATE_CONTEXT MUST take the same form specified in section
[2.2.3.1.8](#smb3_posix_extensions_available).


### 2.2.13 SMB2 CREATE Request

#### 2.2.13.2 SMB2_CREATE_CONTEXT Request Values

Refer to section
[2.2.13.2](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/75364667-3a93-4e2c-b771-592d8d5e876d)
of \[MS-SMB2\] for additional details. This section
only explains details relevant to a SMB2_CREATE_POSIX_CONTEXT request.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>Next</p>
  </td>
 </tr>
 <tr>
  <td colspan="16">
  <p>TagOffset</p>
  </td>
  <td colspan="16">
  <p>TagLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="16">
  <p>Reserved</p>
  </td>
  <td colspan="16">
  <p>BlobOffset</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>BlobLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Buffer
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**Next (4 bytes):** The offset from the beginning of this Create Context to the
beginning of a subsequent 8-byte aligned Create Context. This field MUST be set
to 0 if there are no subsequent contexts.

**TagOffset (2 bytes):** The offset from the beginning of this structure to its
8-byte aligned tag value.

**TagLength (2 bytes):** The length, in bytes, of the Create Context tag.

**Reserved (2 bytes):** This field MUST NOT be used and MUST be reserved. This
value MUST be set to 0 by the client, and ignored by the server.

**BlobOffset (2 bytes):** The offset, in bytes, from the beginning of this
structure to the 8-byte aligned data payload. If BlobLength is 0, the client
SHOULD set this value to 0 and the server MUST ignore it on receipt.

**BlobLength (4 bytes):** The length, in bytes, of the data. The format of the
data is determined by the type of SMB2_CREATE_CONTEXT request, as outlined in
the following sections. The type is inferred from the Create Context name
specified by the **TagOffset** and **TagLength** fields.

**Buffer (variable):** A variable-length buffer that contains the name and data
fields, as defined by **TagOffset**, **TagLength**, **BlobOffset**, and
**BlobLength**. The tag is represented as four or more octets and MUST be one
of the values provided in section
[2.2.13.2](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/75364667-3a93-4e2c-b771-592d8d5e876d)
of \[MS-SMB2\] and the following table. The structure tag indicates
what information is encoded by the data payload. The values specified in section
[2.2.13.2](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/75364667-3a93-4e2c-b771-592d8d5e876d)
of \[MS-SMB2\] and the following extended value are the valid Create Context
values and are defined to be in network byte order.

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Value</p>
   </th>
   <th>
   <p>Meaning</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p>SMB2_CREATE_POSIX_CONTEXT</p>
  <p>0x93AD25509CB411E7B42383DE968BCD7C</p>
  </td>
  <td>
  <p>The data contains a POSIX Create Context.</p>
  </td>
</tr></tbody></table>


##### 2.2.13.2.16 SMB2_CREATE_POSIX_CONTEXT

The SMB2_CREATE_POSIX_CONTEXT context is only specified on an SMB2 CREATE
Request if SMB3_POSIX_EXTENSIONS_AVAILABLE was negotiated. The client MAY
specify SMB2_CREATE_POSIX_CONTEXT if POSIX permissions will be set on create, or
POSIX attributes will be requested from the returned file handle.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>POSIXPerms</p>
  </td>
 </tr>
</table>

**POSIXPerms (4 bytes):** The posix permissions requested on create if
FILE_SUPERSEDE, FILE_CREATE, FILE_OPEN_IF, FILE_OVERWRITE, or FILE_OVERWRITE_IF
are specfied in the
[CreateDisposition](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/e8fb45c1-a03d-44ca-b7ae-47385cfd7997).
The server MUST ignore this value if the file exists.
If the
[CreateDisposition](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/e8fb45c1-a03d-44ca-b7ae-47385cfd7997)
is FILE_OPEN the client MUST set this to 0 and it MUST
be ignored by the server.

#### 2.2.14.2 SMB2_CREATE_CONTEXT Response Values

##### 2.2.14.2.16 SMB2_CREATE_POSIX_CONTEXT Response

The SMB2_CREATE_POSIX_CONTEXT response is sent by the server in response to a
SMB2_CREATE_POSIX_CONTEXT request (section
[2.2.13.2.16](#smb2_create_posix_context)).

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="32">
  <p>NumberOfLinks</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>ReparseTag</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>POSIXPerms</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>OwnerSID (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>GroupSID (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**NumberOfLinks (4 bytes):** The number of hard links to the file.

**ReparseTag (4 bytes):** The reparse tag, as defined in section
[2.1.2.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/c8e77b37-3909-4fe6-a4ea-2b9d423b1ee4)
of [MS-FSCC].

**POSIXPerms (4 bytes):** The posix permissions for the opened file as defined
in section [4.5](https://pubs.opengroup.org/onlinepubs/9699919799/)
of IEEE Std 1003.1-2017.

**OwnerSID (variable):** The owner SID as defined in
[Microsoft Security identifiers](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-identifiers).

**GroupSID (variable):** The group SID as defined in
[Microsoft Security identifiers](https://learn.microsoft.com/en-us/windows-server/identity/ad-ds/manage/understand-security-identifiers).

### 2.2.33 SMB2 QUERY_DIRECTORY Request

Refer to section
[2.2.33](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/10906442-294c-46d3-8515-c277efe1f752)
of \[MS-SMB2\] for additional details. This section
only explains details relevant to a SMB2_FIND_POSIX_INFORMATION request.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="16">
  <p>StructureSize</p>
  </td>
  <td colspan="8">
  <p>FileInformationClass</p>
  </td>
  <td colspan="8">
  <p>Flags</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FileIndex</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FileId</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="16">
  <p>FileNameOffset</p>
  </td>
  <td colspan="16">
  <p>FileNameLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>OutputBufferLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Buffer
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**StructureSize (2 bytes):** The client MUST set this field to 33, indicating
the size of the request structure, not including the header. The client MUST set
this field to this value regardless of how long Buffer[] actually is in the
request being sent.

**FileInformationClass (1 byte):** The file information class describing the
format that data MUST be returned in. Possible values are specified in section
[2.2.33](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/10906442-294c-46d3-8515-c277efe1f752)
of \[MS-SMB2\] as well as the following extended file information class:

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Value</p>
   </th>
   <th>
   <p>Meaning</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p><a href="fscc_posix_extensions.html#fileposixinformation">FilePosixInformation</a></p>
  <p>0x64</p>
  </td>
  <td>
  <p>Basic posix information of a file or directory. Basic information is
  defined as the file's name, creation time, last access time, last modification
  time, last change time, allocation size, size, attributes, inode, devid, link
  count, reparse tag, POSIX permissions, and the owner and group SIDs.
  </p>
  </td>
 </tr></tbody>
</table>

**Flags (1 byte):** Flags indicating how the query directory operation MUST be
processed. As indicated in section [2.2.33](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/10906442-294c-46d3-8515-c277efe1f752)
of \[MS-SMB2\].

**FileIndex (4 bytes):** The byte offset within the directory, indicating the
position at which to resume the enumeration. If SMB2_INDEX_SPECIFIED is set in
Flags, this value MUST be supplied and is based on the FileIndex value received
in a previous enumeration response. Otherwise, it MUST be set to zero and the
server MUST ignore it.

**FileId (16 bytes):** An SMB2_FILEID identifier of the directory on which to
perform the enumeration. This is returned from an SMB2 Create Request to open a
directory on the server.

**FileNameOffset (2 bytes):** The offset, in bytes, from the beginning of the
SMB2 header to the search pattern to be used for the enumeration. This field
MUST be 0 if no search pattern is provided.

**FileNameLength (2 bytes):** The length, in bytes, of the search pattern. This
field MUST be 0 if no search pattern is provided.

**OutputBufferLength (4 bytes):** The maximum number of bytes the server is
allowed to return in the SMB2 QUERY_DIRECTORY Response.

**Buffer (variable):** A variable-length buffer containing the Unicode search
pattern for the request, as described by the **FileNameOffset** and
**FileNameLength** fields. The format, including wildcards and other conventions
for this pattern, is specified in [MS-CIFS] section
[2.2.1.1.3](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-cifs/dc92d939-ec45-40c8-96e5-4c4091e4ab43).

### 2.2.34 SMB2 QUERY_DIRECTORY Response

Refer to section
[2.2.34](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/4f75351b-048c-4a0c-9ea3-addd55a71956)
of \[MS-SMB2\] for additional details. This section
only explains details relevant to a SMB2_FIND_POSIX_INFORMATION request.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="16">
  <p>StructureSize</p>
  </td>
  <td colspan="16">
  <p>OutputBufferOffset</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>OutputBufferLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Buffer
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**StructureSize (2 bytes):** The server MUST set this field to 9, indicating the
size of the request structure, not including the header. The server MUST set
this field to this value regardless of how long Buffer[] actually is in the
request.

**OutputBufferOffset (2 bytes):** The offset, in bytes, from the beginning of
the SMB2 header to the directory enumeration data being returned.

**OutputBufferLength (4 bytes):** The length, in bytes, of the directory
enumeration being returned.

**Buffer (variable):** A variable-length buffer containing the directory
enumeration being returned in the response, as described by the
**OutputBufferOffset** and **OutputBufferLength**. The format of this content
is as specified in [MS-FSCC] section
[2.4](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/4718fc40-e539-4014-8e33-b675af74e3e1),
within the topic for the specific file
information class referenced in the SMB2 QUERY_DIRECTORY Request.
The format for FilePosixInformation is described
[POSIX-FSCC 2.3.1.1](fscc_posix_extensions.html#fileposixinformation).

### 2.2.37 SMB2 QUERY_INFO Request

Refer to section
[2.2.37](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/d623b2f7-a5cd-4639-8cc9-71fa7d9f9ba9)
of \[MS-SMB2\] for additional details. This section
only explains details relevant to a SMB2_FIND_POSIX_INFORMATION request.

<table class="protocol-table">
 <tr>
  <th><p><br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>1<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>2<br>0</p></th>
  <th><p><br>1</p></th>
  <th><p><br>2</p></th>
  <th><p><br>3</p></th>
  <th><p><br>4</p></th>
  <th><p><br>5</p></th>
  <th><p><br>6</p></th>
  <th><p><br>7</p></th>
  <th><p><br>8</p></th>
  <th><p><br>9</p></th>
  <th><p>3<br>0</p></th>
  <th><p><br>1</p></th>
 </tr>
 <tr>
  <td colspan="16">
  <p>StructureSize</p>
  </td>
  <td colspan="8">
  <p>InfoType</p>
  </td>
  <td colspan="8">
  <p>FileInfoClass</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>OutputBufferLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="16">
  <p>InputBufferOffset</p>
  </td>
  <td colspan="16">
  <p>Reserved</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>InputBufferLength</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>AdditionalInformation</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Flags</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>FileId</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>Buffer
  (variable)</p>
  </td>
 </tr>
 <tr>
  <td colspan="32">
  <p>...</p>
  </td>
 </tr>
</table>

**StructureSize (2 bytes):** The client MUST set this field to 41, indicating
the size of the request structure, not including the header. The client MUST set
this field to this value regardless of how long **Buffer**[] actually is in the
request being sent.

**InfoType (1 byte):** The type of information queried. This field MUST contain
one of the values specified in section
[2.2.37](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/d623b2f7-a5cd-4639-8cc9-71fa7d9f9ba9)
of \[MS-SMB2\].

**FileInfoClass (1 byte):** For file information queries, this field MUST
contain one of the FILE_INFORMATION_CLASS values, as specified in
section
[3.3.5.20.1](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/d64e0451-64a2-425a-848e-52a7dddab7b9)
of \[MS-SMB2\] and in \[MS-FSCC\] section
[2.4](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc/4718fc40-e539-4014-8e33-b675af74e3e1),
or the following extension defined in section
[POSIX-FSCC 2.3.1.1].

* [FilePosixInformation](fscc_posix_extensions.html#fileposixinformation)

**OutputBufferLength (4 bytes):** The maximum number of bytes of information the
server can send in the response.

**InputBufferOffset (2 bytes):** The offset, in bytes, from the beginning of the
SMB2 header to the input buffer.

**Reserved (2 bytes):** This field MUST NOT be used and MUST be reserved. The
client MUST set this field to 0, and the server MUST ignore it on receipt.

**InputBufferLength (4 bytes):** The length of the input buffer.

**AdditionalInformation (4 bytes):** Provides additional information to the
server.

**Flags (4 bytes):** The flags MUST be set to a combination of zero or more of
the bit values defined in section
[2.2.37](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/d623b2f7-a5cd-4639-8cc9-71fa7d9f9ba9)
of \[MS-SMB2\] for a FileFullEaInformation query.

**FileId (16 bytes):** An
[SMB2_FILEID](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb2/f1d9b40d-e335-45fc-9d0b-199a31ede4c3)
identifier of the file or named pipe on which to perform the query. Queries for
underlying object store or quota information are directed to the volume on which
the file resides.

**Buffer (variable):** A variable-length buffer containing the input buffer for
the request, as described by the **InputBufferOffset** and **InputBufferLength**
fields.

# 3 Protocol Details

## 3.3 Server Details

### 3.3.5 Processing Events and Sequencing Rules

#### 3.3.5.4 Receiving a SMB2 NEGOTIATE_CONTEXT Request

#### 3.3.5.9 Receiving a SMB2 CREATE Request

#### 3.3.5.18 Receiving a SMB2 QUERY_DIRECTORY Request

#### 3.3.5.20 Receiving a SMB2 QUERY_INFO Request

#### 3.3.5.21 Receiving a SMB2 SET_INFO Request
