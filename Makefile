all: \
	smb3_posix_extensions.html \
	smb3_posix_extensions.pdf \
	fsa_posix_extensions.html \
	fsa_posix_extensions.pdf \
	fscc_posix_extensions.html \
	fscc_posix_extensions.pdf

%.pdf : %-notoc.html
	# Requires wkhtmltopdf with patched qt.
	# Also, generate html without ToC for the PDF as wkhtmltopdf
	# generates brokens links in the PDF.
	wkhtmltopdf --footer-right "[page] / [topage]" --footer-left "SMB3 POSIX Extensions" --footer-line --footer-font-name "Segoe UI" --footer-font-size 10 $< $@

%.html : %.md
	pandoc --from=markdown_mmd -Vcss= -Vpagetitle="SMB3 POSIX Extensions" --standalone --toc --toc-depth=4 --to=html $< > $@

%-notoc.html : %.md
	pandoc --from=markdown_mmd -Vcss= -Vpagetitle="SMB3 POSIX Extensions" --standalone --to=html $< > $@

clean:
	rm *.pdf *.html >/dev/null 2>&1 || echo
