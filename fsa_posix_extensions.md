<style>
body {
  font-family: Segoe UI,SegoeUI,Helvetica Neue,Helvetica,Arial,sans-serif;
  padding: 1em;
  margin: auto;
  max-width: 42em;
  background: #fefefe;
}
.protocol-table p {
  margin-top: 0px;
}
p {
  margin-top: 1rem;
  margin-bottom: 0px;
}
table {
  margin-top: 1rem;
  border: 1px solid #EAEAEA;
  border-collapse: collapse;
  width: 100%;
}
table, th, td {
  border-radius: 3px;
  padding: 5px;
}
th, td {
  font-size: .875rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  border: 1px solid #bbb;
  color: #2a2a2a;
}
th {
  background-color: #ededed;
  font-weight: 600;
}
td {
  padding: 0.5rem;
  background-color: #fff;
}
h1, h2, h3, h4, h5, h6 {
  font-weight: bold;
  color: #000;
}
h1 {
  font-size: 28px;
}
h2 {
  font-size: 24px;
}
h3 {
  font-size: 20px;
}
h4 {
  font-size: 18px;
}
h5 {
  font-size: 16px;
}
h6 {
  font-size: 14px;
}
</style>

# [POSIX-FSA] POSIX Extensions to MS-FSA

Specifies the MS-FSA extensions for supporting POSIX compliant operating
systems.

## Published Version

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody><tr>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p>
  </p>
  </td>
</tr></tbody></table>

## Previous Versions

<table class="protocol-table"><thead>
  <tr>
   <th>
   <p>Date</p>
   </th>
   <th>
   <p>Protocol Revision</p>
   </th>
   <th>
   <p>Revision Class</p>
   </th>
   <th>
   <p>Downloads</p>
   </th>
  </tr>
 </thead><tbody>
 <tr>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p></p>
  </td>
  <td>
  <p>
  </p>
  </td>
 </tr>
</tbody></table>

# 1 Introduction

The SMB3 POSIX Extensions are extensions to enable POSIX compliant
operating systems to better interoperate with SMB3 servers and storage
appliances. This document specified the extensions to the
[[MS-FSA]](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-fscc)
specification. Popular servers such as Samba, Windows Server and others
support SMB3 by default.  These extensions are already implemented in multiple
clients and servers.

# 2 Algorithm Details

## 2.1 Object Store Details

### 2.1.1 Abstract Data Model

### 2.1.3 Initialization

### 2.1.4 Common Algorithms

#### 2.1.4.2 Algorithm for Detecting If Open Files Exist Under a Directory

#### 2.1.4.10 Algorithm for Determining If a Range Access Conflicts with Byte-Range Locks


### 2.1.5 Higher-Layer Triggered Events

#### 2.1.5.1 Server Requests an Open of a File

##### 2.1.5.1.1 Creation of a New File

##### 2.1.5.1.2 Open of an Existing File

#### 2.1.5.4 Server Requests Closing an Open

#### 2.1.5.5 Server Requests Querying a Directory

##### 2.1.5.5.3 Directory Information Queries

###### 2.1.5.5.3.1 FilePosixInformation


#### 2.1.5.7 Server Requests a Byte-Range Lock

#### 2.1.5.8 Server Requests an Unlock of a Byte-Range

#### 2.1.5.11 Server Requests a Query of File Information

##### 2.1.5.11.1 FilePosixInformation


#### 2.1.5.12 Server Requests a Query of File System Information

##### 2.1.5.12.1 FileFsPosixInformation


#### 2.1.5.16 Server Requests Setting of Security Information
